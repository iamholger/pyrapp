def mkLongVector(params, structure):
    """
    Create the parameter combination vector for a particular structure
    """
    import numpy as np
    try:
        dim = len(structure[0])
    except:
        dim=1
    if dim==1:
        return params**structure
    try:
        return np.prod(params**structure, axis=1)
    except:
        return np.prod(params**structure, axis=0) # this is for order 0 things

def sorted_nicely( l ):
    """ Sorts the given iterable in the way that is expected.

    Required arguments:
    l -- The iterable to be sorted.

    """
    import re
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)

# https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length/2136090?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
def chunkIt(seq, num):
    """
    Split a sequence into num lists of approximately the same length
    """
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    # Fix size, sometimes there is spillover
    # TODO: replace with while if problem persists
    if len(out)>num:
        out[-2].extend(out[-1])
        out=out[0:-1]

    if len(out)!=num:
        raise Exception("something went wrong in chunkIt, the target size differs from the actual size")

    return out


# http://stackoverflow.com/questions/3025162/statistics-combinations-in-python
def numCombs(n, k):
    """
    n choose k algorithm
    """
    from operator import mul
    from fractions import Fraction
    return int( reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1) )

def xrandomUniqueCombinations(items, nchoose, howmany=None):
    """ Generator-like function for n choose k items """
    seencombs = []
    # Max number safeguard against infinite loops
    maxnum = numCombs(len(items), nchoose)
    import random
    if howmany is None or howmany > maxnum:
        print("Only %i possible combinations"%maxnum)
        howmany = maxnum
    while len(seencombs) < howmany:
        temp = random.sample(items, nchoose)
        temp.sort()
        if not sorted(temp) in seencombs:
            seencombs.append(temp)
            yield temp

def numCoeffsPoly(dim, order):
    """
    Number of coefficients a dim-dimensional polynomial of order order has.
    """
    ntok = 1
    r = min(order, dim)
    for i in range(r):
      ntok = ntok*(dim+order-i)/(i+1)
    return int(ntok)

def numCoeffsRapp(dim, order):
    """
    Number of coefficients a dim-dimensional polynomial of order order has.
    """
    return 1 + numCoeffsPoly(dim, order[0]) + numCoeffsPoly(dim, order[1])

def possibleOrders(N, dim, mirror=False):
    """
    Utility function to find all possible polynomials
    orders for dataset with N points in N dimension
    """
    from scipy.special import comb
    omax = 0
    while comb(dim + omax+1, omax+1) + 1 <= N: # The '+1' stands for a order 0 polynomial's dof
        omax+=1

    combs = []
    for m in reversed(range(omax+1)):
        for n in reversed(range(m+1)):
            if comb(dim + m, m) + comb(dim+n,n) <= N:
                combs.append((m,n))

    if mirror:
        temp=[tuple(reversed(i)) for i in combs]
        for t in temp:
            if not t in combs:
                combs.append(t)
    return combs

def orders(N, dim, omax):
    return [o for o in possibleOrders(N, dim, mirror=True) if all([x<=omax for x in o])]

def sumorders(N, dim, omax):
    return [o for o in possibleOrders(N, dim, mirror=True) if sum(o)<=omax]

def denomAbsMin(rapp, box, center):
    from scipy import optimize
    return optimize.minimize(lambda x:abs(rapp.denom(x)), center, bounds=box)

def denomMin(rapp, box, center):
    from scipy import optimize
    return optimize.minimize(lambda x:rapp.denom(x), center, bounds=box)

def denomMax(rapp, X):
    import numpy as np
    box=[i for i in zip(np.amin(X, axis=0), np.amax(X, axis=0))]
    center = np.amin(X, axis=0) + 0.5*(np.amax(X, axis=0) - np.amin(X, axis=0))

    from scipy import optimize
    return optimize.minimize(lambda x:-rapp.denom(x), center, bounds=box)



def denomMinMLSL(rapp, box, center, popsize=4, maxeval=1000):
    import numpy as np

    def my_func(x, grad):
        if grad.size > 0:
            _grad = gradient(rapp, x)
            for _i in range(grad.size): grad[_i] = grad[_i]
        return rapp.denom(x)

    import nlopt
    locopt = nlopt.opt(nlopt.LD_MMA, center.size)
    glopt = nlopt.opt(nlopt.GD_MLSL_LDS, center.size)
    glopt.set_min_objective(my_func)
    glopt.set_lower_bounds(np.array([b[0] for b in box]))
    glopt.set_upper_bounds(np.array([b[1] for b in box]))
    glopt.set_local_optimizer(locopt)
    glopt.set_population(popsize)
    glopt.set_maxeval(maxeval)
    xmin = glopt.optimize(center)
    return xmin

def denomMaxMLSL(rapp, box, center, popsize=4, maxeval=1000):
    import numpy as np

    def my_func(x, grad):
        if grad.size > 0:
            _grad = gradient(rapp, x)
            for _i in range(grad.size): grad[_i] = grad[_i]
        return rapp.denom(x)

    import nlopt

    locopt = nlopt.opt(nlopt.LD_MMA, center.size)
    glopt = nlopt.opt(nlopt.GD_MLSL_LDS, center.size)
    glopt.set_max_objective(my_func)
    glopt.set_lower_bounds(np.array([b[0] for b in box]))
    glopt.set_upper_bounds(np.array([b[1] for b in box]))
    glopt.set_local_optimizer(locopt)
    glopt.set_population(popsize)
    glopt.set_maxeval(maxeval)
    xmax = glopt.optimize(center)
    return xmax

def denomChangesSign(rapp, box, center, popsize=4, maxeval=1000):
    xmin = denomMinMLSL(rapp, box, center, popsize, maxeval)
    xmax = denomMaxMLSL(rapp, box, center, popsize, maxeval)
    return rapp.denom(xmin) * rapp.denom(xmax) <0, xmin, xmax

def fit_demo(X, Y, omax=None, threshold=1e-6, verbose=False):
    if omax is None:
        ORDERS = possibleOrders(Y.shape[0], X.shape[1], mirror=True)
    else:
        ORDERS =         orders(Y.shape[0], X.shape[1], omax=omax)

    import numpy as np
    box=zip(np.amin(X, axis=0), np.amax(X, axis=0))
    center = np.amin(X, axis=0) + 0.5*(np.amax(X, axis=0) - np.amin(X, axis=0))

    from pyrapp.rapp import Rapp
    TEST = { o : Rapp(X, Y, order=o) for o in ORDERS}

    from scipy import optimize
    TESTMIN={}
    is_good = []

    import time
    ts = time.time()
    for k, v in TEST.iteritems():
        # Only test the truly rational ones
        if k[1]>0:
            TESTMIN[k] = optimize.minimize(lambda x:abs(v.denom(x)), center, bounds=box)
        else:
            is_good.append(k)
    te = time.time()
    if verbose: print('Root finding took %2.2f s'%((te - ts)))

    has_root =     [ k for k, v in TESTMIN.iteritems() if v["fun"] < threshold]
    is_good.extend([ k for k, v in TESTMIN.iteritems() if v["fun"] > threshold])

    ts = time.time()
    RES = { k : sum([ abs((TEST[k](p) - Y[num])/Y[num]) for num, p in enumerate(X)]) for k in is_good }
    te = time.time()

    if verbose: print('Quality calc took %2.2f s'%((te - ts)))
    import operator
    sorted_res = sorted(RES.items(), key=operator.itemgetter(1))
    winner = sorted_res[0][0]
    assert(winner not in has_root)

    bestp = next(obj for obj in sorted_res if obj[0][1]==0)[0]

    try:
        rootx = next(obj for obj in has_root if obj[0]==winner[0])
    except:
        rootx = next(obj for obj in has_root if obj[1]==winner[1])

    if verbose:
        print("Best approximation: {} {}".format(*winner))
        for i in sorted_res[1:]:
            print("m={} n={} --- residual: {}".format(i[0][0], i[0][1], RES[i[0]]))
        print("Roots found in", has_root)


    return TEST[winner], TEST[bestp], TEST[rootx]

def fit(X, Y, omax=None, threshold=1e-6, verbose=False):
    if omax is None:
        ORDERS = possibleOrders(Y.shape[0], X.shape[1], mirror=True)
    else:
        ORDERS =         sorted(orders(Y.shape[0], X.shape[1], omax=omax))
    if verbose: print("Testing orders", ORDERS)

    from pyrapp.rapp import Rapp
    import time
    ts = time.time()
    R = { o : Rapp(X, Y, order=o) for o in ORDERS}
    te = time.time()
    if verbose: print("Construction took {} seconds".format(te-ts))

    import numpy as np
    box=zip(np.amin(X, axis=0), np.amax(X, axis=0))
    center = np.amin(X, axis=0) + 0.5*(np.amax(X, axis=0) - np.amin(X, axis=0))


    from scipy import optimize
    is_good = []

    D = {}

    import time
    ts = time.time()
    for k, v in R.iteritems():
        # Only test the truly rational ones
        if k[1]>0:
            D[k] = optimize.minimize(lambda x:abs(v.denom(x)), center, bounds=box)
        else:
            D[k]={"fun": 1}
    te = time.time()
    if verbose: print("Denominator root finding took {} seconds".format(te-ts))


    ts = time.time()
    RES = { k : v.residual for k, v in R.iteritems() }
    te = time.time()
    if verbose: print("Residual evaluation took {} seconds".format(te-ts))

    import operator
    sorted_res = sorted(RES.items(), key=operator.itemgetter(1))
    winner = [x for x in sorted_res if D[x[0]]["fun"]>1e-6][0][0]

    if verbose: print("And the winner is", winner)

    return R[winner], winner, RES, D


def plotResidual(fname, winner, RES, D, logy=False):
    import numpy as np
    cmapname   = 'viridis'

    xi, yi,    ci = [], [], []
    bxi, byi, bci = [], [], []
    for k, v in RES.iteritems():
        xi.append(k[0])
        yi.append(k[1])
        ci.append(v)
        if k[1]>0:
            if D[k]["fun"] < 1e-6:
                bxi.append(k[0])
                byi.append(k[1])
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    plt.style.use('ggplot')

    mpl.rc('text', usetex = True)
    mpl.rc('font', family = 'serif', size=12)

    plt.scatter(winner[0], winner[1], marker = '*', c = "magenta",s=400, alpha = 0.7)
    if logy:
        plt.scatter(xi, yi, marker = 'o', c = np.log10(ci), cmap = cmapname, alpha = 0.8)
    else:
        plt.scatter(xi, yi, marker = 'o', c = ci, cmap = cmapname, alpha = 0.8)
    plt.xlabel("$m$")
    plt.ylabel("$n$")
    plt.title("{} --- best: {}".format(fname.replace("_","-").replace("#","-"),  RES[winner]))
    plt.ylim((-0.5,9.5))
    b=plt.colorbar()
    if logy:
        b.set_label("$\log_{10}$ (Resdiual)")
    else:
        b.set_label("(Resdiual)")
    plt.scatter(bxi, byi, marker = 'x', c = "red", alpha = 1.0)
    plt.savefig(fname)

    plt.clf()

def generateStructure_deprecated(dim, order):
    """
    Make structure of exponents for dim-dimensional poly of order order
    """
    import numpy as np
    if (order < 0): raise Exception("Polynomial order %i not implemented"%order)

    structure = [] # This will be returned (vector<vector<int> >)
    zero = np.zeros(dim, dtype=int) # constant offsets in all dimensions vector<int>
    structure.append(list(zero))

    # For constants we are already done
    if order == 0: return np.array(structure)

    # Base vector system, e.g. [1,0,0], [0,1,0], [0,0,1]
    # The linear terms we can also just add to the total structure
    BS=[]
    for d in range(dim):
        p=np.zeros(dim, dtype=int)
        p[d] = 1
        structure.append(list(p))
        BS.append(list(p))

    # Now it gets interesting, adding higher order terms and preventing
    # double counting

    temp = [b for b in BS] # NOTE Make sure changes to temp don't happen in BS!
    for o in range(1, order):
        temp2=[]
        tempstruct=[] # Just a perfomance thing
        # Iterate over base vectors for previous order
        for t in temp:
            # Iterate over trivial base vectors [1,0,0] ...
            for bs in BS:
                from operator import add
                # New element
                e=map(add, t, bs)
                temp2.append(e)
        # Overwrite old structure
        temp=temp2
        # For the next order, we want to add base vectors to each
        # element of the current order
        for v in temp2:
            if v in tempstruct: continue
            tempstruct.append(v)
        structure.extend(tempstruct)
    structure=np.array(structure)

    # Dimension one requires some extra treatment when returning ---writing out is fine
    if dim==1:
        return structure.ravel()

    return np.array(structure)

def mono_next_grlex(m, x):
    #  Author:
    #
    #    John Burkardt
    #     
    #     TODO --- figure out the licensing thing https://people.sc.fsu.edu/~jburkardt/py_src/monomial/monomial.html

    #  Find I, the index of the rightmost nonzero entry of X.
    i = 0
    for j in range(m, 0, -1):
        if 0 < x[j-1]:
            i = j
            break

    #  set T = X(I)
    #  set X(I) to zero,
    #  increase X(I-1) by 1,
    #  increment X(M) by T-1.
    if i == 0:
        x[m-1] = 1
        return x
    elif i == 1:
        t = x[0] + 1
        im1 = m
    elif 1 < i:
        t = x[i-1]
        im1 = i - 1

    x[i-1] = 0
    x[im1-1] = x[im1-1] + 1
    x[m-1] = x[m-1] + t - 1

    return x

def genStruct(dim, mnm):
    while True:
        yield mnm
        mnm =  mono_next_grlex(dim, mnm)

def generateStructure(dim, order):
    import numpy as np
    import pyrapp, copy
    ncmax = pyrapp.numCoeffsPoly(dim, order)
    gen = genStruct(dim, np.zeros(dim))
    structure = np.array([ copy.copy(next(gen)) for _ in range(ncmax)], dtype=int)
    # Dimension one requires some extra treatment when returning ---writing out is fine
    if dim==1:
        return structure.ravel()
    return structure

def partialPoly(R, Punscaled, coord, denom=True):
    """
    Partial of denominator or numerator polynomial of rational approx
    by coordinate coord evaluated at (unscaled) parameter point.
    """
    import numpy as np
    structure = R._struct_h if denom else R._struct_g
    coeffs    = R._bcoeff   if denom else R._acoeff
    P = R._scaler(Punscaled)
    grad = [0] # Derivative of constant term

    if R.dim==1:
        for s in structure[1:]: # Start with the linear terms
            grad.append((s*P[0]**(s-1))/(R._scaler._Xmax[0] - R._scaler._Xmin[0])*2)

    else:
        for s in structure[1:]: # Start with the linear terms
            if s[coord] == 0: # The partial derivate evaluates to 0 if you do "d/dx of  y"
                grad.append(0)
                continue
            temp = 1.0
            for i in range(len(s)): # x,y,z,...
                if i==coord:
                    temp*=s[i]
                    temp*=P[i]**(s[i]-1)/(R._scaler._Xmax[i] - R._scaler._Xmin[i])*2 # Jacobian factor of 2/(b - a) here since we scaled into [-1,1]
                else:
                    temp*=P[i]**s[i]
            grad.append(temp)
    return np.dot(grad, coeffs)

def gradient(R, Punscaled, denom=True):
    """
    Gradeitn of denominator or numerator polynomial of rational approx
    evaluated at (unscaled) parameter point.
    """
    return [partialPoly(R, Punscaled, i, denom=denom) for i in range(R.dim)]

