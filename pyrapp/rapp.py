import numpy as np

#https://medium.com/pythonhive/python-decorator-to-measure-the-execution-time-of-methods-fa04cb6bb36d
def timeit(method):
    import time
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed

class Rapp(object):
    def __init__(self, *args, **kwargs):
        """
        Multivariate rational approximation f(x)_mn =  g(x)_m/h(x)_n

        kwargs:
            fname --- to read in previously calculated Pade approximation

            X     --- anchor points
            Y     --- function values
            order --- tuple (m,n) m being the order of the numerator polynomial --- if omitted: auto
        """
        # NOREAD=False
        import os
        # if not os.path.exists("__profcache__"):
            # try:
                # os.makedirs("__profcache__")
            # except:
                # NOREAD=True
                # pass
        # else:
        # self._noread=kwargs["no_read"] if kwargs.get("no_read") is not None else False
        self._noread=True
        if len(args) == 0:
            pass
        else:
            if type(args[0])==dict:
                self.mkFromDict(args[0])
            elif type(args[0]) == str:
                self.mkFromJSON(args[0])
            else:
                self._Y   = np.array(args[1], dtype=np.float64)
                from pyrapp import scaler
                a=kwargs["a"] if kwargs.get("a") is not None else -1
                b=kwargs["b"] if kwargs.get("b") is not None else 1
                self._scaler = scaler.Scaler(args[0], a, b)
                self.mkFromData(kwargs=kwargs)

    def mkFromJSON(self, fname):
        import json
        d = json.load(open(fname))
        self.mkFromDict(d)

    def mkFromDict(self, pdict):
        self._acoeff     = np.array(pdict["acoeff"])
        self._bcoeff     = np.array(pdict["bcoeff"])
        from pyrapp import scaler
        try:
            self._scaler = scaler.Scaler(pdict["scaler"])
        except:
            self._scaler = None
        self.setStructures(pdict["m"], pdict["n"])

    def mkFromData(self, kwargs):
        """
        Calculate the Pade approximation
        """
        order=kwargs["order"]
        debug=kwargs["debug"] if kwargs.get("debug") is not None else False
        strategy=int(kwargs["strategy"]) if kwargs.get("strategy") is not None else 2
        self.calc(order[0], order[1], debug=debug, strategy=strategy)

    def setStructures(self, m, n):
        self._struct_g = self.mkStructure(self.dim, m)
        self._struct_h = self.mkStructure(self.dim, n)
        import pyrapp.tools as prt
        self._M        = prt.numCoeffsPoly(self.dim, m)
        self._N        = prt.numCoeffsPoly(self.dim, n)
        self._m        = m
        self._n        = n
        self._K=m+n+1

    @property
    def domain(self):
        """
        The interpolation domain as min/max pairs in the unscaled world.
        """
        return [(a, b) for a,b in zip(self._scaler._Xmin, self._scaler._Xmax)]

    @property
    def domain_scaled(self):
        """
        The interpolation domain as min/max pairs in the scaled world.
        """
        return [(self._scaler._a, self._scaler._b) for i in range(self.dim)]

    @property
    def center(self):
        """
        The geometric center of the interpolation domain in the unscaled world.
        """
        return self._scaler._Xmin + 0.5*(self._scaler._Xmax - self._scaler._Xmin)

    @property
    def center_scaled(self):
        """
        The geometric center of the interpolation domain in the scaled world.
        """
        return np.array([a + 0.5*(b-a) for a,b in zip(self._scaler._Xmin, self._scaler._Xmax)])

    # @timeit
    def coeffSolve(self, VM, VN):
        """
        This does the solving for the numerator and denominator coefficients
        following Anthony's recipe.
        """
        Fmatrix=np.diag(self._Y)
        # rcond changes from 1.13 to 1.14
        rcond = -1 if np.version.version < "1.15" else None
        MM, res, rank, s  = np.linalg.lstsq(VM, Fmatrix, rcond=rcond)
        Zmatrix = MM.dot(VN)
        U, S, V = np.linalg.svd(VM.dot(Zmatrix) - Fmatrix.dot(VN))
        self._bcoeff = V[-1]
        self._acoeff = Zmatrix.dot(self._bcoeff)

    # @timeit
    def coeffSolve2(self, VM, VN):
        """
        This does the solving for the numerator and denominator coefficients
        following Steve's recipe.
        """
        Feps = - (VN.T * self._Y).T
        # the full left side of the equation
        y = np.hstack([ VM, Feps[:,1:self._Y.size ] ])
        U, S, V = np.linalg.svd(y)
        # manipulations to solve for the coefficients
        # Given A = U Sigma VT, for A x = b, x = V Sigma^-1 UT b
        tmp1 = np.transpose( U ).dot( np.transpose( self._Y ))[0:S.size]
        Sinv = np.linalg.inv( np.diag(S) )
        x = np.transpose(V).dot( Sinv.dot(tmp1) )
        self._acoeff = x[0:self._M]
        self._bcoeff = np.concatenate([np.array([1.00]),x[self._M:self._M+self._N+1]])

    def coeffSolve3(self, VM, VN):
        """
        Steve's code
        # The equation
        #   1  x1  x1**2 ... x1**m  -fm1 -fm1*x1  -fm1*x1**2 ... -fm1*x1**n  \
        #   1  x2  x2**2 ... x2**m  -fm2 -fm2*x2  -fm2*x2**2 ... -fm2*x2**n   |
        #   ...                                                               |
        #   1  xK  xK**2 ... xK**m  -fmK -fmK*xK  ....           -fmK*xK**n  /
        #
        #   l = (a0, a1, ..., am, b0, b1, ..., bn)^T
        """

        self.coeffSolve2(VM, VN)
        import pyrapp
        hasPoles, xmin, xmax = pyrapp.tools.denomChangesSign(self, self.domain, self.center, popsize=10000)
        if hasPoles:
            print("there is a pole!")
        else:
            return

        searchbox = [sorted((xmin[i], xmax[i])) for i in range(self.dim)]
        searchcenter = np.array([a + 0.5*(b-a) for a, b in searchbox])
        optpole = pyrapp.tools.denomAbsMin(self, searchbox, searchcenter)
        xpole = optpole["x"]
        print("at", xpole, self(xpole), "fmin/fmax:", self.denom(xmin), self.denom(xmax))

        import pyrapp.tools as prt

        Fmatrix=np.diag(self._Y)

        Xvector=self._scaler.scaledPoints

        VM0 = np.zeros(VM.shape)
        # insert a set of weights and epsilon values


        from scipy import optimize

        bvector = self._Y
        Feps = - (VN.T * bvector).T

        # y = np.hstack([ VM, Feps[:,:self._Y.size ] ])
        y = np.hstack([ VM, Feps ])

        c = np.hstack([ VM0, VN ])
        # from IPython import embed
        # embed()

        # right side of the equation -- part of the constraints
        constr = np.ones(self._Y.size)
        #delta*np.sum(np.abs(y)**2, axis=-1)**(1./2.)

        # minimize the norm of the coefficients!
        def loss(x, sign=1.):
            suml = np.sum( y*x, axis=1 )
            return sign*np.sum( np.power(np.abs(suml),2) )

        def jac(x, sign=1.):
            suml = np.array(2.0*np.sum( y*x, axis=1 ))
            return 2.0* x.T * y.T * y

        cons = {'type':'ineq', # x here is the coefficients!
                'fun':lambda x: np.dot(c,x) - constr.T} # This is constrained to be >0
#                'jac':lambda x: y}
        opt = {'disp':False}

        # random starting point for coefficients -- do better than just normal random?
        x0 = np.random.randn(self._M+self._N)

        # from IPython import embed
        # embed()
        # solver
        res_cons = optimize.minimize(loss, x0, constraints = cons,
                        method='SLSQP', options = opt )
        # the coefficients
        x = res_cons['x']

        # translated into Holgerese
        self._acoeff = x[0:self._M]
        self._bcoeff = x[self._M:self._M+self._N+1]



        import pyrapp
        hasPoles, xmin, xmax = pyrapp.tools.denomChangesSign(self, self.domain, self.center, popsize=10000)
        if hasPoles:
            print("there is still a pole!")
        else:
            print("Fixed!")
        # print("starting root finding")

        # def f(x,b):
            # lv_h = self.mkLongVector(x, self._struct_h)
            # h=np.asarray(b).dot(lv_h)
            # return np.abs(h)

        # def vf(x):
            # print(f(x))
            # return np.array([f(x),0.0])

        # print(f(self.center_scaled,self._bcoeff))
        # sol = optimize.minimize(f, self.center_scaled, bounds=self.domain_scaled, args=(self._bcoeff), jac=False)
# #        sol = optimize.fsolve(f, center)
        # if sol.fun < 1e-6: print("optimize",sol.x,sol.fun)

        #

        # # large number
        # dev = np.finfo(np.float64).max

        # # initial set of weights per equation (data point)
        # wt = np.ones(self._Y.size)
        # # initial set of residual signs (i.e. +1 or -1) per data point
        # ee = np.ones(self._Y.size)

        # VM0 = np.zeros(VM.shape)
        # # maximum deviation
        # devmax = 0
        # # initial error estimate
        # error = 0.0001*min(abs(self._Y))
        # # relative error estimate
        # error_rel = 0.0001
        # # possible penalty term
        # delta = .001
        # from scipy import optimize

        # bplus   = np.multiply(wt,self._Y + error_rel*np.sqrt(np.abs(self._Y)) )
        # bminus  = np.multiply(wt,self._Y - error_rel*np.sqrt(np.abs(self._Y)) )

        # Fminus = - (VN.T * bminus).T
        # Fplus  = (VN.T * bplus).T

        # # the full left side of the equation
        # yup = np.hstack([ VM, Fminus[:,1:VN.size] ])
        # ydn = np.hstack([-VM,  Fplus[:,1:VN.size] ])
        # y = np.vstack([ yup, ydn ])

        # # right side of the equation -- part of the constraints
        # constr = np.hstack( [bminus, -bplus] )
        # #delta*np.sum(np.abs(y)**2, axis=-1)**(1./2.)

        # # minimize the norm of the coefficients!
        # def loss(x, sign=1.):
            # return sign*np.sum( np.abs(x)**2 )

        # def jac(x, sign=1.):
            # return sign*x

        # cons = {'type':'ineq',
                # 'fun':lambda x: np.dot(y,x) - constr}
# #                'jac':lambda x: y}
        # opt = {'disp':False}

        # # random starting point for coefficients -- do better than just normal random?
        # x0 = np.random.randn(self._M+self._N-1)

        # # solver
        # res_cons = optimize.minimize(loss, x0, jac=jac,constraints = cons,
                        # method='SLSQP', options = opt )
        # # the coefficients
        # x = res_cons['x']

        # # translated into Holgerese
        # self._acoeff = x[0:self._M]
        # self._bcoeff = np.concatenate([np.array([1.00]),x[self._M:self._M+self._N+1]])

    def calc(self, m, n, debug=False, strategy=2):
        """
        Do everything
        """
        # Set M, N, K, polynomial structures
        # n_required=self.numCoeffs(self.dim, m+n+1)
        import pyrapp.tools as prt
        import time
        n_required = prt.numCoeffsRapp(self.dim, (m,n))
        if n_required > self._Y.shape[0]:
            raise Exception("Not enough inputs: got %i but require %i to do m=%i n=%i"%(n_required, Fmatrix.shape[0], m,n))

        Xvector=self._scaler.scaledPoints

        ts=time.time()
        self.setStructures(m,n)
        te=time.time()
        if debug:
            print("structure setting took {} seconds".format(te-ts))

        ts=time.time()
        VanderMonde=self.mkVandermonde(Xvector, self._K)
        VM = VanderMonde[:, 0:(self._M)]
        VN = VanderMonde[:, 0:(self._N)]
        te=time.time()
        if debug:
            print("VM took {} seconds".format(te-ts))

        if   strategy==1: self.coeffSolve( VM, VN)
        elif strategy==2: self.coeffSolve2(VM, VN)
        elif strategy==3: self.coeffSolve3(VM, VN)
        else: raise Exception("calc() strategy %i not implemented"%strategy)


    # PYTHON version of function in libProfessor2
    def mkVandermonde(self, params, order):
        import pyrapp.tools as prt
        PM = np.zeros((len(params), prt.numCoeffsPoly(self.dim, order)), dtype=np.float64)

        s = self.mkStructure(self.dim, order)
        for a, p in enumerate(params):
            PM[a]=self.mkLongVector(p, s)
        return PM

    # PYTHON version of function in libProfessor2
    def mkStructure(self, dim, order):
        """
        Make structure of exponents for dim-dimensional poly of order order
        """
        if order < 0: raise Exception("Polynomial order %i not implemented"%order)

        if order == 0: return np.array(list(np.zeros(dim, dtype=int)))

        import pyrapp.tools as prt

        if self._noread is False:
            import os
            nc = prt.numCoeffsPoly(dim, order)

            fname ="%s/structures/structure_%i"%(os.path.dirname(prt.__file__), dim)

            if os.path.exists(fname):
                S = np.genfromtxt(fname, dtype=int, max_rows=nc)
                if S.shape[0] != nc:
                    print("Whoopsidoodles, I expected to get {} coeffs but instead found {}".format(nc, S.shape[0]))
                    print ("Not cached, calculating and overwriting {}".format(fname))
                    S=prt.generateStructure(dim, order)
                    np.savetxt(fname, S, fmt="%i")

            else:
                print("Whoopsidoodles")
                print ("Not cached, calculating and overwriting {}".format(fname))
                S=prt.generateStructure(dim, order)
                np.savetxt(fname, S, fmt="%i")

        else:
            S=prt.generateStructure(dim, order)

        return S


    # PYTHON version of function in libProfessor2
    def mkLongVector(self, params, structure):
        """
        Create the parameter combination vector for a particular structure
        """
        if self.dim==1:
            return params**structure
        try:
            return np.prod(params**structure, axis=1)
        except:
            return np.prod(params**structure, axis=0) # this is for order 0 things

    @property
    def dim(self):
        try:
            return self._scaler.dim
        except:
            return 2

    @property
    def M(self): return self._M

    @property
    def N(self): return self._N

    @property
    def m(self): return self._m

    @property
    def n(self): return self._n

    @property
    def residual(self):
        res = [ abs((self.numer(self._scaler._XS[num], scale=False)/self.denom(self._scaler._XS[num], scale=False) - y) / y) for num, y in enumerate(self._Y) if y!=0]
        if len(res)==0: return -1
        return sum(res)*(self._acoeff.shape[0]+self._bcoeff.shape[0])/len(res)

    def denom(self, Xraw, scale=True):
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        lv_h = np.array(self.mkLongVector(X, self._struct_h))
        h=self._bcoeff.dot(lv_h)
        return h

    def numer(self, Xraw, scale=True):
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        lv_g = np.array(self.mkLongVector(X, self._struct_g))
        g=self._acoeff.dot(lv_g)
        return g

    def __call__(self, Xraw, scale=True):
        """
        Operator, assume x is a scaled point.
        """
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        den = self.denom(X, scale=False)
        if den==0:
            return 0
        else:
            return self.numer(X, scale=False)/self.denom(X, scale=False)

    @property
    def pmin(self): return self._scaler._Xmin

    @property
    def pmax(self): return self._scaler._Xmax

    @property
    def asDict(self):
        """
        Store all info in dict as basic python objects suitable for JSON
        """
        d={}
        d["m"]    = self.m
        d["n"]    = self.n
        d["acoeff"] = list(self._acoeff)
        d["bcoeff"] = list(self._bcoeff)
        d["scaler"] = self._scaler.asDict
        return d

    def save(self, fname):
        import json
        with open(fname, "w") as f:
            json.dump(self.asDict, f)

if __name__=="__main__":

    import sys

    def mkTestData(NX, dim=1):
        def anthonyFunc(x):
            return (10*x)/(x**3 - 4* x + 5)
        NR = 1
        np.random.seed(555)
        X = np.random.rand(NX, dim)*10
        Y = np.array([anthonyFunc(*x) for x in X])
        return X, Y

    X, Y = mkTestData(500)
    r=Rapp(X,Y, order=(1,3))

    import pylab
    pylab.plot(X, Y, marker="*", linestyle="none", label="Data")
    TX = sorted(X)
    YW = [r(p) for p in TX]

    pylab.plot(TX, YW, label="Rational approx m={} n={}".format(1,3))
    pylab.legend()
    pylab.xlabel("x")
    pylab.ylabel("f(x)")
    pylab.savefig("demo.pdf")

    sys.exit(0)
