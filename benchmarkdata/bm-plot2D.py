import numpy as np


def plotRes(f_rapp, f_test, f_out):
    R = readRapp(f_rapp)
    X_test, Y_test = readData(f_test)
    res = [abs(R(x, scale=False)-Y_test[num]) for num, x in enumerate(X_test)]
    
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.rc('text', usetex = True)
    mpl.rc('font', family = 'serif', size=12)
    mpl.style.use("ggplot")
    cmapname   = 'viridis'
    plt.clf()

    plt.scatter(X_test[:,0], X_test[:,1], marker = '.', c = np.ma.log10(res), cmap = cmapname, alpha = 0.8)
    plt.vlines(-1, ymin=-1, ymax=1, linestyle="dashed")
    plt.vlines( 1, ymin=-1, ymax=1, linestyle="dashed")
    plt.hlines(-1, xmin=-1, xmax=1, linestyle="dashed")
    plt.hlines( 1, xmin=-1, xmax=1, linestyle="dashed")
    plt.xlabel("$x$")
    plt.ylabel("$y$")
    plt.ylim((-1.5,1.5))
    plt.xlim((-1.5,1.5))
    b=plt.colorbar()
    b.set_label("$\log_{10}$ (Residual)")
    plt.savefig(f_out)

def readRapp(fname):
    import pyrapp
    return pyrapp.Rapp(fname)

def mkAndStoreRapp(fin, fout, order=(3,3)):
    X, Y = readData(fin)
    import pyrapp
    R=pyrapp.Rapp(X,Y, order=order)
    R.save(fout)

def readData(fname):
    import numpy as np
    D = np.loadtxt(fname, delimiter=",")
    X=D[:,[0,1]]
    Y=D[:,-1]
    return X, Y

if __name__=="__main__":
    import optparse, os, sys
    op = optparse.OptionParser(usage=__doc__)
    op.add_option("-o", dest="OUTFILE", default="plot.pdf", help="Output file name (default: %default)")
    opts, args = op.parse_args()

    # Build
    # mkAndStoreRapp(args[0], "test.json")

    plotRes(args[0],  args[1], opts.OUTFILE)
