import numpy as np
import pylab

class Rapp(object):
    def __init__(self, *args, **kwargs):
        """
        Multivariate rational approximation f(x)_mn =  g(x)_m/h(x)_n

        kwargs:
            fname --- to read in previously calculated Pade approximation

            X     --- anchor points
            Y     --- function values
            order --- tuple (m,n) m being the order of the numerator polynomial --- if omitted: auto
        """
        NOREAD=False
        import os
        # if not os.path.exists("__profcache__"):
            # try:
                # os.makedirs("__profcache__")
            # except:
                # NOREAD=True
                # pass
        # else:
            # NOREAD=kwargs["no_read"] if kwargs.get("no_read") is not None else False
        self._noread=False#NOREAD
        if type(args[0])==dict:
            self.mkFromDict(args[0])
        elif type(args[0]) == str:
            self.mkFromJSON(args[0])
        else:
            self._Y   = np.array(args[1], dtype=np.float64)
            from pyrapp import scaler
            self._scaler = scaler.Scaler(args[0],0,1)
            self.mkFromData(kwargs=kwargs)

    def mkFromJSON(self, fname):
        # This breaks atm because of missing scaler
        import json
        d = json.load(open(fname))
        self.mkFromDict(d)

    def mkFromDict(self, pdict):
        # This breaks atm because of missing scaler
        self._acoeff     = np.array(pdict["acoeff"])
        self._bcoeff     = np.array(pdict["bcoeff"])
        from pyrapp import scaler
        self._scaler = scaler.Scaler(pdict["scaler"])
        self.setStructures(pdict["m"], pdict["n"])

    def mkFromData(self, kwargs):
        """
        Calculate the Pade approximation
        """
        order=kwargs["order"]
        self.calc(order[0], order[1])

    def setStructures(self, m, n):
        self._struct_g = self.mkStructure(self.dim, m)
        self._struct_h = self.mkStructure(self.dim, n)
        import pyrapp.tools as prt
        self._M        = prt.numCoeffsPoly(self.dim, m)
        self._N        = prt.numCoeffsPoly(self.dim, n)
        # self._M        = self.numCoeffs(self.dim, m)
        # self._N        = self.numCoeffs(self.dim, n)
        self._m        = m
        self._n        = n
        self._K=m+n+1

    def calc(self, m, n):
        """
        Do everything
        """
        # Set M, N, K, polynomial structures
        # n_required=self.numCoeffs(self.dim, m+n+1)
        import pyrapp.tools as prt
        n_required = prt.numCoeffsRapp(self.dim, (m,n))
#        print(X)
#        print(Y)
        # large number
        dev = np.finfo(np.float64).max

        Fmatrix=np.diag(self._Y)
        
        Xvector=self._scaler.scaledPoints
#        print(Xvector)
#        print(Fmatrix)
        if  n_required > Fmatrix.shape[0]:
            raise Exception("Not enough (%i vs %i) inputs to do m=%i n=%i"%(n_required, Fmatrix.shape[0], m,n))
        self.setStructures(m,n)

        VanderMonde=self.mkVandermonde(Xvector, self._K)
        VM = VanderMonde[:, 0:(self._M)]
        VM0 = VM*0
        VN = VanderMonde[:, 0:(self._N)]
        # insert a set of weights and epsilon values

        # rcond changes from 1.13 to 1.14
        rcond = -1 if np.version.version < "1.15" else None

        from scipy import optimize
        # The equation
        #   1  x1  x1**2 ... x1**m  -fm1 -fm1*x1  -fm1*x1**2 ... -fm1*x1**n  \      
        #   1  x2  x2**2 ... x2**m  -fm2 -fm2*x2  -fm2*x2**2 ... -fm2*x2**n   | 
        #   ...                                                               | 
        #   1  xK  xK**2 ... xK**m  -fmK -fmK*xK  ....           -fmK*xK**n  /      
        #
        #   l = (a0, a1, ..., am, b0, b1, ..., bn)^T

        bvector = Y
        Feps = - (VN.T * bvector).T        

        y = np.hstack([ VM, Feps[:,:Y.size ] ])

        c = np.hstack([ VM0, VN ])

        # right side of the equation -- part of the constraints
        constr = np.ones(Y.size)
        #delta*np.sum(np.abs(y)**2, axis=-1)**(1./2.)

        # minimize the norm of the coefficients!
        def loss(x, sign=1.):
            suml = np.sum( y*x, axis=1 )
            return sign*np.sum( np.power(np.abs(suml),2) )            

        def jac(x, sign=1.):
            suml = np.array(2.0*np.sum( y*x, axis=1 ))
            return 2.0* x.T * y.T * y
        
        cons = {'type':'ineq',
                'fun':lambda x: np.dot(c,x) - constr.T}
#                'jac':lambda x: y}
        opt = {'disp':False}

        # random starting point for coefficients -- do better than just normal random?
        x0 = np.random.randn(self._M+self._N)

        # solver
        res_cons = optimize.minimize(loss, x0, constraints = cons,
                        method='SLSQP', options = opt )
        # the coefficients
        x = res_cons['x']

        # translated into Holgerese
        self._acoeff = x[0:self._M]
        self._bcoeff = x[self._M:self._M+self._N+1]          

        box=zip(np.amin(0*Xvector, axis=0), np.amax(0*Xvector+1, axis=0))        
        center = np.amin(Xvector, axis=0) + 0.5*(np.amax(Xvector, axis=0) - np.amin(Xvector, axis=0))
        print("starting root finding")
        
        def f(x,b):
            lv_h = self.mkLongVector(x, self._struct_h)
            h=np.asarray(b).dot(lv_h)
            return np.abs(h)

        def vf(x):
            print(f(x))
            return np.array([f(x),0.0])

        print(f(center,self._bcoeff))
        sol = optimize.minimize(f, center, bounds=box, args=(self._bcoeff), jac=False)
#        sol = optimize.fsolve(f, center)
        if sol.fun < 1e-6: print("optimize",sol.x,sol.fun)

    # PYTHON version of function in libProfessor2
    def mkVandermonde(self, params, order):
        import pyrapp.tools as prt
        PM = np.zeros((len(params), prt.numCoeffsPoly(self.dim, order)), dtype=np.float64)

        s = self.mkStructure(self.dim, order)
        for a, p in enumerate(params):
            PM[a]=self.mkLongVector(p, s)
        return PM

    # PYTHON version of function in libProfessor2
    def mkStructure(self, dim, order):
        """
        Make structure of exponents for dim-dimensional poly of order order
        """
        if order < 0: raise Exception("Polynomial order %i not implemented"%order)

        # if self._noread is False:
            # try:
                # structure=np.loadtxt("__profcache__/structure_%i_%i"%(dim, order))
                # return structure
            # except:
                # pass

        if order == 0: return np.array(list(np.zeros(dim, dtype=int)))

        try:
            import pyrapp.tools as prt
            import os
            nc = prt.numCoeffsPoly(dim, order)
            if nc < 2500:
                S = np.genfromtxt("%s/structures/structure_%i"%(os.path.dirname(prt.__file__), dim), dtype=int, max_rows=nc)
                # print "from file","%s/structures/structure_%i"%(os.path.dirname(prt.__file__), dim)
                return S
            else:
                raise Exception("Not implemented")
        except Exception as e:
            print("Whoopsidoodles")
            print e



        structure = [] # This will be returned (vector<vector<int> >)
        zero = np.zeros(dim, dtype=int) # constant offsets in all dimensions vector<int>
        structure.append(list(zero))

        # For constants we are already done
        if order == 0: return np.array(structure)

        # Base vector system, e.g. [1,0,0], [0,1,0], [0,0,1]
        # The linear terms we can also just add to the total structure
        BS=[]
        for d in range(dim):
            p=np.zeros(dim, dtype=int)
            p[d] = 1
            structure.append(list(p))
            BS.append(list(p))

        # Now it gets interesting, adding higher order terms and preventing
        # double counting

        temp = [b for b in BS] # NOTE Make sure changes to temp don't happen in BS!
        for o in range(1, order):
            temp2=[]
            tempstruct=[] # Just a perfomance thing
            # Iterate over base vectors for previous order
            for t in temp:
                # Iterate over trivial base vectors [1,0,0] ...
                for bs in BS:
                    from operator import add
                    # New element
                    e=map(add, t, bs)
                    temp2.append(e)
            # Overwrite old structure
            temp=temp2
            # For the next order, we want to add base vectors to each
            # element of the current order
            for v in temp2:
                if v in tempstruct: continue
                tempstruct.append(v)
            structure.extend(tempstruct)
        structure=np.array(structure)

        # Store structure in cache
        np.savetxt("__profcache__/structure_%i_%i"%(dim, order), structure, fmt="%i")

        # Dimension one requires some extra treatment when returning ---writing out is fine
        if self.dim==1:
            return structure.ravel()

        return np.array(structure)

    # PYTHON version of function in libProfessor2
    def mkLongVector(self, params, structure):
        """
        Create the parameter combination vector for a particular structure
        """
        if self.dim==1:
            return np.power(params,structure)
        try:
            return np.prod(np.power(params,structure), axis=1)
        except:
            return np.prod(np.power(params,structure), axis=0) # this is for order 0 things

    @property
    def dim(self): return self._scaler.dim

    @property
    def M(self): return self._M

    @property
    def N(self): return self._N

    @property
    def m(self): return self._m

    @property
    def n(self): return self._n

    @property
    def residual(self):
        res = [ abs((self.numer(self._scaler._XS[num], scale=False)/self.denom(self._scaler._XS[num], scale=False) - y) / y) for num, y in enumerate(self._Y) if y>0]
        return sum(res)*(self._acoeff.shape[0]+self._bcoeff.shape[0])/len(res)

    @property
    def residualvec(self):
        res = [ self.numer(self._scaler._XS[num], scale=False)/self.denom(self._scaler._XS[num], scale=False) - y for num, y in enumerate(self._Y) ]
        return np.array(res)

    def denom(self, Xraw, scale=True):
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        lv_h = np.array(self.mkLongVector(X, self._struct_h))
        h=self._bcoeff.dot(lv_h)
        return h

    def numer(self, Xraw, scale=True):
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        lv_g = np.array(self.mkLongVector(X, self._struct_g))
        g=self._acoeff.dot(lv_g)
        return g

    def __call__(self, Xraw, scale=True):
        """
        Operator, assume x is a scaled point.
        """
        assert(len(Xraw)==self.dim)
        X = self._scaler(Xraw) if scale else Xraw
        return self.numer(X, scale=False)/self.denom(X, scale=False)

    @property
    def pmin(self): return self._scaler._Xmin

    @property
    def pmax(self): return self._scaler._Xmax

    @property
    def asDict(self):
        """
        Store all info in dict as basic python objects suitable for JSON
        """
        d={}
        d["m"]    = self.m
        d["n"]    = self.n
        d["acoeff"] = list(self._acoeff)
        d["bcoeff"] = list(self._bcoeff)
        d["scaler"] = self._scaler.asDict
        return d

    def save(self, fname):
        import json
        with open(fname, "w") as f:
            json.dump(self.asDict, f)

if __name__=="__main__":

    import sys

    def mkTestData(NX, dim=2):
        # def anthonyFunc(x):
        # return (10*x**5+1)/(x**3 - 4* x + 5)
        def anthonyFunc(x, y):
            return (10*x**2+1+x*y**2 -y**2)/(x**5 - 4* x + 5*y +1-y**2+200)
        np.random.seed(554)
        X = np.random.rand(NX, dim)*10
        Y = np.array([anthonyFunc(*x) for x in X])
        return X, Y
    
    # def mkTestData(NX, dim=1):
    #     def anthonyFunc(x):
    #         return (10*x)/(x**3 - 4* x + 5)
    #     NR = 1
    #     np.random.seed(555)
    #     X = np.random.rand(NX, dim)*10
    #     Y = np.array([anthonyFunc(*x) for x in X])
    #     return X, Y

    X, Y = mkTestData(84)

    R = {}

    import time
    ts = time.time()
    for m in range(1,10):
        for n in range(1,10):
            if m+n > 15: continue
            try:
                ta = time.time()
                R[(m,n)] = Rapp(X,Y, order=(m,n))
                print m,n,time.time()-ta,R[(m,n)].residual
            except Exception as e:
                print e
                from IPython import embed
                embed()
                sys.exit(1)
    te = time.time()

    print("Construction took {} seconds".format(te-ts))
    
    box=zip(np.amin(X, axis=0), np.amax(X, axis=0))
    center = np.amin(X, axis=0) + 0.5*(np.amax(X, axis=0) - np.amin(X, axis=0))


    from scipy import optimize
    is_good = []

    D = {}

    ts = time.time()
    for k, v in R.iteritems():
        # Only test the truly rational ones
        if k[1]>0:
            D[k] = optimize.minimize(lambda x:abs(v.denom(x)), center, bounds=box)
        else:
            D[k]={"fun": 1}
    te = time.time()
    print("Denominator root finding took {} seconds".format(te-ts))


    ts = time.time()
    RES = { k : v.residual for k, v in R.iteritems() }
    te = time.time()
    print("Residual evaluation took {} seconds".format(te-ts))

    import operator
    sorted_res = sorted(RES.items(), key=operator.itemgetter(1))
    winner = [x for x in sorted_res if D[x[0]]["fun"]>1e-7][0][0]
    wres = RES[winner]
    
    start=winner
    
    medres = np.median(RES.values())
    
    mnow, nnow = winner
    print mnow, nnow, RES[(mnow,nnow)]
    while mnow>=1 and nnow>=1:
        mnow-=1
        if nnow>0: nnow-=1
        if mnow == 0: continue
        if nnow == 0: continue
        print mnow, nnow, RES[(mnow,nnow)], D[(mnow,nnow)]["fun"]
        if D[(mnow,nnow)]["fun"]<1e-6: continue
        if RES[(mnow,nnow)]/wres > 10 :continue
        # if RES[(mnow,nnow)]>medres:
        # print "no"
        # continue
        winner=(mnow,nnow)

    print "Winner: m={} n={}".format(*winner)
    t = R[winner]

    print t.asDict["acoeff"]
    print t.asDict["bcoeff"]

    cmapname   = 'viridis'

    xi, yi,    ci = [], [], []
    bxi, byi, bci = [], [], []
    for k, v in RES.iteritems():
        xi.append(k[0])
        yi.append(k[1])
        ci.append(v)
        if k[1]>0:
            if D[k]["fun"] < 1e-6:
                bxi.append(k[0])
                byi.append(k[1])


    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.rc('text', usetex = True)
    mpl.rc('font', family = 'serif', size=12)

    plt.scatter(xi, yi, marker = 'o', c = np.log10(ci), cmap = cmapname, alpha = 0.8)
    plt.xlabel("$m$")
    plt.ylabel("$n$")
    plt.ylim((-0.5,9.5))
    b=plt.colorbar()
    b.set_label("$\log_{10}$ (Resdiual)")
    plt.scatter(bxi, byi, marker = 'x', c = "red", alpha = 1.0)
    plt.scatter(start[0], start[1], marker = '*', c = "cyan",s=400, alpha = 0.7)
    plt.scatter(winner[0], winner[1], marker = '*', c = "magenta",s=400, alpha = 0.7)
    plt.savefig('test03-residual.pdf')

    plt.clf()

    xi, yi, ci = [], [], []
    for k, v in D.iteritems():
        xi.append(k[0])
        yi.append(k[1])
        ci.append(v["fun"])
    plt.scatter(xi, yi, marker = 'o', c = np.log10(ci), cmap = cmapname, alpha = 0.8)
    plt.xlabel("$m$")
    plt.ylabel("$n$")
    plt.ylim((-0.5,9.5))
    b=plt.colorbar()
    b.set_label("$\log_{10}$ (abs denominator minimum)")
    plt.savefig('test03-denommin.pdf')

    
    # import time
    # ts = time.time()

    # m = 2
    # n = 3
    
    # r=Rapp(X,Y, order=(2,3))

    # te = time.time()    
    # print("Construction took {} seconds".format(te-ts))    

    # import pylab
    # pylab.plot(X, Y, marker="*", linestyle="none", label="Data")
    # TX = sorted(X)
    # YW = [r(p) for p in TX]

    # pylab.plot(TX, YW, label="Rational approx m={} n={}".format(m,n))
    # pylab.legend()
    # pylab.xlabel("x")
    # pylab.ylabel("f(x)")
    # pylab.savefig("demo.pdf")

    # sys.exit(0)
