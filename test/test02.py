import pyrapp
import numpy as np

def testFunc(x, y):
    return (x**3 - x*y + y**3)/(x*y**2)

def testFuncNoise(x, y, noise=0):
    return np.random.normal(1,noise)*(x**3 - x*y + y**3)/(x*y**2)

def mkTestData(NX, dim=2):
    def anthonyFunc(x,y):
        return (10*x+4*y**2 - 3*x*y)/(x**3 - 4* y + 5)
    np.random.seed(555)
    X = np.random.rand(NX, dim)*10
    Y = np.array([anthonyFunc(*x) for x in X])
    return X, Y

x = np.linspace(0.1, 1, 30)
y = np.linspace(0.1, 1, 30)
X, Y = np.meshgrid(x, y)
Z = testFuncNoise(X, Y, noise=0.05)
C = testFunc(X, Y)

_P = np.array(zip(X.ravel(), Y.ravel()))
_Z = Z.ravel()


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# ax.set_title("$ f(x,y) = \\frac{x^3-xy +y^3}{xy^2}$ noise level %.1f"%noiseinfo["NOISE"])
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.scatter(X, Y, Z, c="r", s=2)



r, p, s = pyrapp.tools.fit_demo(_P, _Z, omax=6, verbose=True, threshold=1e-8)
x = np.linspace(0.1, 0.95, 21)
y = np.linspace(0.1, 0.95, 21)
X, Y = np.meshgrid(x, y)
_P = np.array(zip(X.ravel(), Y.ravel()))



V=np.array([r(_p) for _p in _P])
__Z = V.reshape((21,21))

ax.plot_wireframe(X, Y,  __Z, rstride=1, cstride=1, label="Rapp")

plt.show()
