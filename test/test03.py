import pyrapp
import numpy as np

def mkTestData(NX, dim=2):
    # def anthonyFunc(x):
        # return (10*x**5+1)/(x**3 - 4* x + 5)
    def anthonyFunc(x, y):
        return (10*x**2+1+x*y**2 -y**2)/(x**5 - 4* x + 5*y +1-y**2+200)
    np.random.seed(554)
    X = np.random.rand(NX, dim)*10
    Y = np.array([anthonyFunc(*x) for x in X])
    return X, Y

X, Y = mkTestData(300)



from pyrapp.rapp import Rapp

R = {}

import time
ts = time.time()
for m in range(10):
    for n in range(10):
        # if m+n > 15: continue
        try:
            R[(m,n)] = Rapp(X,Y, order=(m,n))
        except Exception as e:
            print(e)
            from IPython import embed
            embed()
            sys.exit(1)
te = time.time()

print("Construction took {} seconds".format(te-ts))

box=zip(np.amin(X, axis=0), np.amax(X, axis=0))
center = np.amin(X, axis=0) + 0.5*(np.amax(X, axis=0) - np.amin(X, axis=0))


from scipy import optimize
is_good = []

D = {}

ts = time.time()
for k, v in R.iteritems():
    # Only test the truly rational ones
    if k[1]>0:
        D[k] = optimize.minimize(lambda x:abs(v.denom(x)), center, bounds=box)
    else:
        D[k]={"fun": 1}
te = time.time()
print("Denominator root finding took {} seconds".format(te-ts))


ts = time.time()
RES = { k : v.residual for k, v in R.iteritems() }
te = time.time()
print("Residual evaluation took {} seconds".format(te-ts))

import operator
sorted_res = sorted(RES.items(), key=operator.itemgetter(1))
winner = [x for x in sorted_res if D[x[0]]["fun"]>1e-6][0][0]
wres = RES[winner]

start=winner

medres = np.median(RES.values())

mnow, nnow = winner
# print mnow, nnow, RES[(mnow,nnow)]
while mnow>=1 and nnow>=1:
    mnow-=1
    if nnow>0: nnow-=1
    # print mnow, nnow, RES[(mnow,nnow)]
    if D[(mnow,nnow)]["fun"]<1e-6: continue
    if RES[(mnow,nnow)]/wres > 10 :continue
    # if RES[(mnow,nnow)]>medres:
        # print "no"
        # continue
    winner=(mnow,nnow)

# print "Winner: m={} n={}".format(*winner)

cmapname   = 'viridis'

xi, yi,    ci = [], [], []
bxi, byi, bci = [], [], []
for k, v in RES.iteritems():
    xi.append(k[0])
    yi.append(k[1])
    ci.append(v)
    if k[1]>0:
        if D[k]["fun"] < 1e-6:
            bxi.append(k[0])
            byi.append(k[1])


import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('text', usetex = True)
mpl.rc('font', family = 'serif', size=12)
mpl.style.use("ggplot")

plt.scatter(xi, yi, marker = 'o', c = np.log10(ci), cmap = cmapname, alpha = 0.8)
plt.xlabel("$m$")
plt.ylabel("$n$")
plt.ylim((-0.5,9.5))
b=plt.colorbar()
b.set_label("$\log_{10}$ (Resdiual)")
plt.scatter(bxi, byi, marker = 'x', c = "red", alpha = 1.0)
plt.scatter(start[0], start[1], marker = '*', c = "cyan",s=400, alpha = 0.7)
plt.scatter(winner[0], winner[1], marker = '*', c = "magenta",s=400, alpha = 0.7)
plt.savefig('test03-residual.pdf')

plt.clf()

xi, yi, ci = [], [], []
for k, v in D.iteritems():
    xi.append(k[0])
    yi.append(k[1])
    ci.append(v["fun"])
plt.scatter(xi, yi, marker = 'o', c = np.log10(ci), cmap = cmapname, alpha = 0.8)
plt.xlabel("$m$")
plt.ylabel("$n$")
plt.ylim((-0.5,9.5))
b=plt.colorbar()
b.set_label("$\log_{10}$ (abs denominator minimum)")
plt.savefig('test03-denommin.pdf')
