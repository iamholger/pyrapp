import numpy as np
import pyrapp

import sys
m=int(sys.argv[1])
n=int(sys.argv[2])
noise=float(sys.argv[3])

np.random.seed(1)
xmax=float(sys.argv[4])

import testData
X, Y = testData.mkTestData(1, 51 , order=(m,n), noise=noise, xmax=xmax)

Y-=1

R=pyrapp.Rapp(X,Y,order=(m,n+1), strategy=3) # This is cool, fitting with higher than actually generated order in denominator is almost guaranteed to give you a pole, at least in a noisy world
Rpole=pyrapp.Rapp(X,Y,order=(m,n+1), strategy=2) # This is cool, fitting with higher than actually generated order in denominator is almost guaranteed to give you a pole, at least in a noisy world

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('text', usetex = True)
mpl.rc('font', family = 'serif', size=12)
mpl.style.use('ggplot')

f, axarr = plt.subplots(2,1,sharex=True, gridspec_kw = {'height_ratios':[3, 1]})


ax = axarr[0]

ax.scatter(X,Y, label="Data generated with m={} n={}".format(m,n))
ax.plot(X, [Rpole(x) for x in X], "m", label="R. App. m={} n ={} that has a pole".format(m,n+1))
ax.plot(X, [R(x) for x in X], "b", label="Fixed R. App. m={} n ={}".format(m,n+1))
ax.legend(loc=1)
ax.set_ylabel("f(x)")

ax = axarr[1]
ax.axhline(1, color="k")
ax.plot(X, [R(x)/Y[num] for num, x in enumerate(X)], "b")
ax.plot(X, [Rpole(x)/Y[num] for num, x in enumerate(X)], "m")
ax.set_ylim((0.2,2))
ax.set_xlabel("x")
ax.set_ylabel("Ratio")
plt.subplots_adjust(wspace = 0.1, hspace = 0.1)
fname='test07_{}_{}.pdf'.format(str(m).zfill(2),n)
plt.savefig(fname)
print("File written to {}".format(fname))
