import pyrapp
import numpy as np

def mkTestData(NX, dim=1):
    def anthonyFunc(x):
        return (10*x)/(x**3 - 4* x + 5)
    NR = 1
    np.random.seed(555)
    X = np.random.rand(NX, dim)*10
    Y = np.array([anthonyFunc(*x) for x in X])
    return X, Y

X, Y = mkTestData(500)

r, p, s = pyrapp.tools.fit_demo(X, Y, omax=10, verbose=True)

import pylab
pylab.plot(X, Y, marker="*", linestyle="none", label="Data")
X = sorted(np.random.rand(1000, 1)*10)

YR = [r(x) for x in X]
YP = [p(x) for x in X]
YS = [s(x) for x in X]

pylab.plot(X, YR, label="Rational approx m={} n={}".format(r.m, r.n))
pylab.plot(X, YP, label="Polynomial approx m={}".format(p.m))
pylab.plot(X, YS, label="Rational approx with pole m={} n={}".format(s.m, s.n))
pylab.legend()
pylab.xlabel("x")
pylab.ylabel("f(x)")
dist = max(Y)-min(Y)
pylab.ylim( (min(Y)-0.2*dist, max(Y)+0.2*dist ))
pylab.savefig("test01.pdf")
