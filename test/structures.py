import numpy as np

def mono_next_grlex ( m, x ):
#  Author:
#
#    John Burkardt
#     
#     TODO --- figure out the licensing thing https://people.sc.fsu.edu/~jburkardt/py_src/monomial/monomial.html
#
#
#
#  Find I, the index of the rightmost nonzero entry of X.
#
  i = 0
  for j in range ( m, 0, -1 ):
    if ( 0 < x[j-1] ):
      i = j
      break

#  set T = X(I)
#  set X(I) to zero,
#  increase X(I-1) by 1,
#  increment X(M) by T-1.
#
  if ( i == 0 ):
    x[m-1] = 1
    return x
  elif ( i == 1 ):
    t = x[0] + 1
    im1 = m
  elif ( 1 < i ):
    t = x[i-1]
    im1 = i - 1

  x[i-1] = 0
  x[im1-1] = x[im1-1] + 1
  x[m-1] = x[m-1] + t - 1

  return x

def genStruct(dim, mnm):
    while True:
        yield mnm
        mnm =  mono_next_grlex(dim, mnm)

def generateStructure(dim, order):
    import numpy as np
    import pyrapp, copy
    ncmax = pyrapp.numCoeffsPoly(dim, order)
    gen = genStruct(dim, np.zeros(dim))
    return np.array([ copy.copy(next(gen)) for _ in range(ncmax)], dtype=int)

def generateStructure_old(dim, order):
    """
    Make structure of exponents for dim-dimensional poly of order order
    """
    import numpy as np
    if (order < 0): raise Exception("Polynomial order %i not implemented"%order)

    structure = [] # This will be returned (vector<vector<int> >)
    zero = np.zeros(dim, dtype=int) # constant offsets in all dimensions vector<int>
    structure.append(list(zero))

    # For constants we are already done
    if order == 0: return np.array(structure)

    # Base vector system, e.g. [1,0,0], [0,1,0], [0,0,1]
    # The linear terms we can also just add to the total structure
    BS=[]
    for d in range(dim):
        p=np.zeros(dim, dtype=int)
        p[d] = 1
        structure.append(list(p))
        BS.append(list(p))

    # Now it gets interesting, adding higher order terms and preventing
    # double counting

    temp = [b for b in BS] # NOTE Make sure changes to temp don't happen in BS!
    for o in range(1, order):
        temp2=[]
        tempstruct=[] # Just a perfomance thing
        # Iterate over base vectors for previous order
        for t in temp:
            # Iterate over trivial base vectors [1,0,0] ...
            for bs in BS:
                from operator import add
                # New element
                e=map(add, t, bs)
                temp2.append(e)
        # Overwrite old structure
        temp=temp2
        # For the next order, we want to add base vectors to each
        # element of the current order
        for v in temp2:
            if v in tempstruct: continue
            tempstruct.append(v)
        structure.extend(tempstruct)
    structure=np.array(structure)

    # Dimension one requires some extra treatment when returning ---writing out is fine
    if dim==1:
        return structure.ravel()

    return np.array(structure)

if __name__ == "__main__":
    import pyrapp, copy
    import time
    ts=time.time()
    for dim in xrange(1,21):
        omax=50
        ncmax = pyrapp.numCoeffsPoly(dim, omax)
        if ncmax < 11000: o = omax
        else:
            while ncmax>11000:
                omax-=1
                ncmax = pyrapp.numCoeffsPoly(dim, omax)

        S = generateStructure(dim, omax)

        print dim, omax, ncmax
        np.savetxt("structures/structure_%i"%(dim), S, fmt="%i")
    te=time.time()
    print "Took {} seconds".format(te-ts)
