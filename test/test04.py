import numpy as np
import pyrapp

import sys
m=int(sys.argv[1])
n=int(sys.argv[2])

np.random.seed(1)

X=sorted(np.random.rand(100, 1))
Y=np.array([(x)/(1+x +x**2) for x in X]).ravel()

R=pyrapp.Rapp(X,Y,order=(m,n))

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('text', usetex = True)
mpl.rc('font', family = 'serif', size=12)
mpl.style.use('ggplot')

f, axarr = plt.subplots(2,1,sharex=True, gridspec_kw = {'height_ratios':[3, 1]})


ax = axarr[0]

ax.scatter(X,Y, label="Data")
ax.plot(X, [R(x) for x in X], "b", label="R. App. m={} n ={} $N_\mathrm{{coeff}}={}$".format(m,n, pyrapp.numCoeffsRapp(1, (m,n))))
ax.legend(loc=4)
ax.set_ylim((0,0.36))
ax.set_ylabel("f(x)")
# ax.set_xlim((-0.1,10.1))

ax = axarr[1]
ax.axhline(1, color="k")
ax.plot(X, [R(x)/Y[num] for num, x in enumerate(X)], "b")
ax.set_ylim((0.8,1.2))
ax.set_xlabel("x")
ax.set_ylabel("Ratio")
# ax.set_xlim((0,10))
# from IPython import embed
# embed()
plt.subplots_adjust(wspace = 0.1, hspace = 0.1)
fname='test04_{}_{}.pdf'.format(str(m).zfill(2),n)
plt.savefig(fname)
print("File written to {}".format(fname))
