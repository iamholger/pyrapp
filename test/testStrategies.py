import pyrapp
import numpy as np

if __name__=="__main__":

    np.random.seed(44)
    import sys
    import testData

    dim = int(sys.argv[1])

    X, Y = testData.mkTestData(dim, int(sys.argv[2]), (3,2), noise=float(sys.argv[3]))

    R1={}
    R2={}

    A=-1
    B=1

    for m in range(1,4):
        for n in range(1,4):
            if pyrapp.numCoeffsRapp(int(sys.argv[1]),  (m,n))> int(sys.argv[2]): continue
            r1=pyrapp.Rapp(X,Y, order=(m,n), debug=False, strategy=1, a=A, b=B)
            r2=pyrapp.Rapp(X,Y, order=(m,n), debug=False, strategy=2, a=A, b=B)
            R1[(m,n)] = r1
            R2[(m,n)] = r2

    from IPython import embed
    embed()

    if dim ==1:
        import pylab
        xp = np.linspace(X.min(), X.max(), 101)
        pylab.plot(X,Y, "b.", linestyle="none")
        for k in R1.keys():
            pylab.plot(xp,[R1[k]([x]) for x in xp], label=k)
        pylab.ylim( (Y.min(), Y.max()) )
        pylab.legend()
        pylab.savefig("testStrategies_1.pdf")
        pylab.clf()
        pylab.plot(X,Y, "b.", linestyle="none")
        for k in R2.keys():
            pylab.plot(xp,[R2[k]([x]) for x in xp], label=k)
        pylab.ylim( (Y.min(), Y.max()) )
        pylab.legend()
        pylab.savefig("testStrategies_2.pdf")
