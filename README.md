# pyrapp

Calculation of multivariate Rational APProximations with python.
The approximations are evaluated in a monomial basis.
The code detects and ignores solutions with spurious poles.

