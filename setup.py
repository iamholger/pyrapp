from setuptools import setup, find_packages
setup(
  name = 'pyrapp',
  version = '0.6.5',
  description = 'Multivariate (monomial basis) rational approximation',
  url = 'https://bitbucket.org/iamholger/pyrapp',
  author = 'Holger Schulz',
  author_email = 'hschulz@fnal.gov',
  packages = find_packages(),
  include_package_data = True,
  install_requires = [
    'numpy',
    'scipy',
    'sobol',
    'pyDOE'
  ],
  scripts=['bin/rapp-fit','bin/rapp-fit-mpi',  'bin/rapp-ls'],
  extras_require = {
  },
  entry_points = {
  },
  dependency_links = [
  ]
)
